#include "Utils.h"

#include <string>
#include <iostream>

void setColor(Color color)
{
	std::string colorStr;

	switch( color )
	{
	case BLACK:
		colorStr = "\033[0;30m";
		break;
	case BLUE:
		colorStr = "\033[0;34m";
		break;
	case RED:
		colorStr = "\033[0;31m";
		break;
	case GREEN:
		colorStr = "\033[0;32m";
		break;
	case CYAN:
		colorStr = "\033[0;36m";
		break;
	case YELLOW:
		colorStr = "\033[0;33m";
		break;
	case END:
		colorStr = "\033[0m";
		break;
	}

	std::cout << colorStr;
}

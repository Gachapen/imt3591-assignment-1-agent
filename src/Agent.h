/*
 * Agent.h
 *
 *  Created on: Feb 5, 2013
 *      Author: magnus
 */

#ifndef AGENT_H_
#define AGENT_H_

#include <iostream>
#include <string>
#include "Environment.h"

class Agent {
public:
	enum Action { SUCK, LEFT, RIGHT, UP, DOWN, NO_OP };

	Agent(const std::string& name);
	virtual ~Agent();

	virtual Action perceiveAndAct(Environment::CellState state, Position position) = 0;
	virtual void reset() = 0;
	bool loadTexture();
	SDL_Surface* getTexture() const;
	const std::string& getName() const;
private:
	SDL_Surface* texture;
	std::string name;
};

class HorizontalScanAgent: public Agent {
public:
	HorizontalScanAgent();

	Action perceiveAndAct(Environment::CellState state, Position position);
	void reset();

protected:
	enum State {
		TURNED_RIGHT,
		TURNED_LEFT,
		WALKING_RIGHT,
		WALKING_LEFT,
		MOVE_TO_TOP_FROM_RIGHT,
		MOVE_TO_TOP_FROM_LEFT
	};

	Position previousLocation;
	State currentState;
	bool firstMove;
};

class HorizontalScanAgentPlus: public Agent {
public:
	HorizontalScanAgentPlus();
	Action perceiveAndAct(Environment::CellState state, Position position);
	void reset();

private:
	int rightmost, leftmost, topmost, bottommost;
	bool rightmostSet, leftmostSet, topmostSet, bottommostSet;

	enum State {
		WALKING_RIGHT,
		WALKING_LEFT,
		WALKING_UP
	};

	enum Mode {
		HORIZONTAL_SCAN,
		VERTICAL_SCAN,
		ALL_KNOWN
	};

	enum UpFrom {
		UPFROM_RIGHT,
		UPFROM_LEFT,
		UPFROM_NONE
	};

	Position previousLocation;
	State currentState;
	Mode currentMode;
	Action previousAction;
	bool firstMove;
	UpFrom upFrom;

	Action horizontalScanMode(Environment::CellState state, Position position);
	Action verticalScanMode(Environment::CellState state, Position position);
	Action allKnownMode(Environment::CellState state, Position position);
};

class ReflexAgent: public Agent {
public:
	ReflexAgent();

	Action perceiveAndAct(Environment::CellState state, Position position);
	void reset();
};

class ReflexStateAgent: public Agent {
public:
	ReflexStateAgent();

	Action perceiveAndAct(Environment::CellState state, Position position);
	void reset();
private:
	bool cellAClean;
	bool cellBClean;
};

#endif /* AGENT_H_ */

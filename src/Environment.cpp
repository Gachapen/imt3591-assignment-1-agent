#include "Environment.h"
#include "Utils.h"
#include <iostream>
#include "Agent.h"

Environment::Environment(int columns, int rows):
	numColumns(columns),
	numRows(rows),
	cellImage(nullptr),
	dirtImage(nullptr),
	wallImage(nullptr),
	cellWidth(0),
	cellHeight(0)
{
	createMap(columns, rows);
}

Environment::~Environment()
{
	deleteMap();

	// Free images
	SDL_FreeSurface(cellImage);
	SDL_FreeSurface(dirtImage);
}

bool Environment::load_assets()
{
	wallImage = IMG_Load("images/wall.jpg");

	if( wallImage == nullptr )
	{
		setColor(RED);
		std::cout << "ERROR! Could not load cell image\n";
		setColor(END);
	}


	dirtImage = IMG_Load("images/dirt.jpg");

	if( dirtImage == nullptr )
	{
		setColor(RED);
		std::cout << "ERROR! Could not load dirt image\n";
		setColor(END);

		return false;
	}


	cellImage = SDL_LoadBMP("images/cell.bmp");

	if( cellImage == nullptr )
	{
		setColor(RED);
		std::cout << "ERROR! Could not load cell image\n";
		setColor(END);

		return false;
	}

	cellWidth = cellImage->w;
	cellHeight = cellImage->h;

	return true;
}

bool Environment::hasDirtyCell(int column, int row) const
{
	if (map[row][column] == DIRTY) {
		return true;
	}

	return false;
}

void Environment::setCleanCell(int column, int row)
{
	map[row][column] = CLEAN;
}

void Environment::draw(SDL_Surface* screen) const
{
	for (int row = 0; row < numRows; row++) {
		for (int column = 0; column < numColumns; column++) {
			SDL_Rect cellRect;
			cellRect.x = cellWidth * column;
			cellRect.y = cellHeight * row;
			SDL_BlitSurface(cellImage, nullptr, screen, &cellRect);

			if (hasDirtyCell(column, row)) {
				SDL_Rect dirtRect;
				dirtRect.x = (cellWidth * column) + (cellWidth / 2.0f) - (dirtImage->w / 2.0f);
				dirtRect.y = (cellHeight * row) + (cellHeight / 2.0f) - (dirtImage->h / 2.0f);
				SDL_BlitSurface(dirtImage, nullptr, screen, &dirtRect);
			}

			if( map[row][column] == WALL ) {
				SDL_BlitSurface( wallImage, nullptr, screen, &cellRect );
			}
		}
	}

	drawObject(agent->getTexture(), screen, agentPos);
}

void Environment::setDirtyCell(int column, int row)
{
	if( row > numRows || column > numColumns)
	{
		setColor(RED);
		std::cout << "Cannot assign dirt to cells which do not exist\n"
				"Current cells are\n"
				"Rows = " << numRows << "\n"
				"Columns = " << numColumns << "\n";
		setColor(END);

		return;
	}

	map[row][column] = DIRTY;
}

int Environment::getNumCleanCells() const
{
	int numCleanCells = 0;

	for (int row = 0; row < numRows; row++) {
		for (int column = 0; column < numColumns; column++) {
			if (map[row][column] == CLEAN) {
				numCleanCells++;
			}
		}
	}

	return numCleanCells;
}

void Environment::setSize(int columns, int rows)
{
	deleteMap();

	this->numRows = rows;
	this->numColumns = columns;
	createMap(columns, rows);
}

void Environment::drawObject(SDL_Surface* object, SDL_Surface* screen, Position position) const
{
	SDL_Rect objectPosition;
	objectPosition.x = (position.column * cellWidth) - (object->w / 2.0f) + (cellWidth / 2.0f);
	objectPosition.y = (position.row * cellHeight) - (object->h / 2.0f) + (cellHeight / 2.0f);
	SDL_BlitSurface(object, nullptr, screen, &objectPosition);
}

void Environment::deleteMap()
{
	for (int row = 0; row < numRows; row++) {
		delete[] map[row];
	}

	delete[] map;
}

void Environment::placeRandomDirt(const int probability)
{
	for (int row = 0; row < numRows; row++) {
		for (int column = 0; column < numColumns; column++) {
			int random = rand() % 101;
			if (random < probability) {
				map[row][column] = DIRTY;
			}
		}
	}

	setColor(GREEN);
	std::cout << "Dirt spawned with a probability of " << probability << "%\n";
	setColor(END);
}

void Environment::load_map(const std::string& fileName)
{
	std::string line;

	std::ifstream infile(fileName);

	if( !infile )
	{
		setColor(RED);
		std::cout << "ERROR! Could not load file " << fileName << "\n";
		setColor(END);

		return;
	}

	infile >> line;

	int createColumns = 0;
	int createRows = 0;

	while( infile )
	{
		if (line[0] == '#')
		{
			infile.ignore(256, '\n');
		}
		else if (line == "column")
		{
			if( !(infile >> createColumns))	// numColumns
			{
				return;
			}
		}
		else if (line == "row")
		{
			if( !(infile >> createRows) )		// numRows
			{
				return;
			}
		}
		else if (line[0] == '1' || line[0] == '0')
		{
			if( createRows != 0 && createColumns != 0 )
			{
				numRows = createRows;
				numColumns = createColumns;

				createMap(numColumns, numRows);

				for( int i = 0; i < numRows; i++ )
				{
					for( int j = 0; j < numColumns; j++ )
					{
						if( line[j] == '1' )
							map[i][j] = WALL;
					}
					infile >> line;
				}

				setColor(GREEN);
				std::cout << "Map finished loading...\n";
				setColor(END);
				return;

			} // end if (legit input)
			else
			{
				setColor(RED);
				std::cout << "ERROR! " << fileName << " is damaged\n";
				std::cout << "Using previous settings\n";
				setColor(END);

				return;
			}
		} // end if (1 or 0 at start of line)

		infile >> line;
	} // end of while (something to read from file)
}

Position Environment::getRandomMapPosition() const
{
	Position pos;
	pos.row = rand() % numRows;
	pos.column = rand() % numColumns;
	return pos;
}

void Environment::setAgent(Agent* agent)
{
	this->agent = agent;
	agentPos = getRandomMapPosition();
}

void Environment::setAgentPosition(const Position position)
{
	agentPos = position;
}


int Environment::update()
{
	Agent::Action action = agent->perceiveAndAct(map[agentPos.row][agentPos.column], agentPos);
	int moves = 0;

	switch (action) {
	case Agent::Action::SUCK:
		map[agentPos.row][agentPos.column] = CLEAN;
		moves = 0;
		break;

	case Agent::Action::LEFT:
		std::cout << "LEFT\n";
		if (agentPos.column > 0 ) {
		       if (map[agentPos.row][agentPos.column-1] != WALL ) {
				agentPos.column--;
		       }
		}
		moves = 1;
		break;
	case Agent::Action::RIGHT:
		if (agentPos.column < numColumns - 1 ) {
			if (map[agentPos.row][agentPos.column+1] != WALL ) {
				agentPos.column++;
			}
		}
		moves = 1;
		break;

	case Agent::Action::DOWN:
		if (agentPos.row < numRows - 1 ) {
			if( map[agentPos.row+1][agentPos.column] != WALL ) {
				agentPos.row++;
			}
		}
		moves = 1;
		break;

	case Agent::Action::UP:
		std::cout << "UP\n";
		if (agentPos.row > 0 ) {
		       if (map[agentPos.row-1][agentPos.column] != WALL ) {
			agentPos.row--;
		       }
		}
		moves = 1;
		break;

	case Agent::Action::NO_OP:
		moves = 0;
		break;

	default:
		moves = 0;
		break;
	}

	return moves;
}

void Environment::createMap(int columns, int rows)
{
	// Create a new 2D array.
	map = new CellState*[rows];
	for (int row = 0; row < rows; row++) {
		map[row] = new CellState[columns];
	}
}

bool Environment::isValidCell(const int row, const int column) const
{
	if( row > numRows || column > numColumns )
	{
		setColor(RED);
		std::cout << "Cannot assign cleaner to a cell which does not exist\n"
				"Current cells are\n"
				"Rows = " << numRows << "\n"
				"Columns = " << numColumns << "\n";
		setColor(END);

		return false;
	}

	return true;
}

void Environment::cleanMap()
{
	for (int row = 0; row < numRows; row++) {
		for (int column = 0; column < numColumns; column++) {
			map[row][column] = CLEAN;
		}
	}
}

void Environment::display_status()
{
	//--------------------------------
	setColor(CYAN);
	std::cout << "Number of columns";
	setColor(END);

	std::cout << " = ";

	setColor(GREEN);
	std::cout << numColumns << "\n";
	setColor(END);

	//--------------------------------
	setColor(CYAN);
	std::cout << "Number of rows";
	setColor(END);

	std::cout << " = ";

	setColor(GREEN);
	std::cout << numRows << "\n";
	setColor(END);
}

bool Environment::load_config(std::ifstream& infile, std::string line)
{
	if( line == "numberOfColumns")
	{
		if( !(infile >> numColumns) )
			return false;
		else
			return true;
	}
	else if( line == "numberOfRows")
	{
		if( !(infile >> numRows) )
			return false;
		else
			return true;
	}

	return false;
}

void Environment::save_config(std::ofstream& outfile)
{
	outfile << "environmentSize " << numRows << " " << numColumns << "\t# (rows columns)\n";
}

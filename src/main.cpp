#include <SDL/SDL.h>
#include "App.h"
#include <string>
#include <stdio.h>

int main(int argc, char** argv)
{
	App theApp;

	std::string configFileName("default.conf");

	for( int i = 1; i < argc; i++ )
	{
		std::string parameter(argv[i]);

		if( parameter == "--config" || parameter == "-c" )
		{
			configFileName.assign(argv[i+1]);


		}
	}

	//std::cout << configFileName;
	return theApp.execute(configFileName);
}

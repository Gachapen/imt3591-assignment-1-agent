#ifndef UTILS_H_
#define UTILS_H_

enum Color {
	BLACK,
	BLUE,
	RED,
	GREEN,
	CYAN,
	YELLOW,
	END
};

void setColor(Color color);

struct Position {
	int row;
	int column;

	Position():
		row(0),
		column(0)
	{ }

	Position(int row, int column) :
		row(row),
		column(column)
	{ }

	bool operator == (const Position& right) {
		return (this->row == right.row && this->column == right.column);
	}

	bool operator != (const Position& right) {
		return (this->row != right.row || this->column != right.column);
	}
};

#endif /* UTILS_H_ */


#ifndef APP_H_
#define APP_H_

#include <SDL/SDL.h>
#include <SDL/SDL_thread.h>
#include <fstream>
#include "Environment.h"
#include "Agent.h"

class App
{
	int m_screenWidth;
	int m_screenHeight;

	Uint32 m_framesPerSecond;
	Uint32 m_prevFrameStartTime;
	Uint32 m_curFrameStartTime;

	bool m_fpsController;
	unsigned int m_simulationSteps;
	bool m_renderOn;
	bool m_respawnDirt;
	int m_respawnDirtProbability;

	std::string m_currentConfigFile;

	SDL_Surface* m_screen;

	Environment m_environment;
	Agent* m_agent;

	void init();
	bool initSdl();
	void handleResize(const int width, const int height);
	void runSimulation();
	void render();
	void changeEnvironmentSize();
	void placeDirt();
	void placeRandomDirt();

	/*
	 * Toggles the FPS on and off.
	 */
	void toggle_fps();

	/*
	 * Sets the FPS from user input, returns from the function if
	 * the input was not an integer and gives a error message.
	 * Also warns the user if fps has been toggled off.
	 */
	void set_fps();
	void set_steps();
	void toggle_render();
	void setVacuumerPosition();
	void set_respawn_probability();
	void display_status();
	bool load_config(std::string fileName);
	void create_default_config(std::string fileName);
	void save_config(std::string fileName);
	void set_screen_height();
	void set_screen_width();
	void display_agent_list();
	void set_agent();
public:
	App();
	virtual ~App();

	int execute(const std::string& configFileName);
};

#endif /* APP_H_ */

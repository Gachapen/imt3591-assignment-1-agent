/*
 * Agent.cpp
 *
 *  Created on: Feb 5, 2013
 *      Author: magnus
 */

#include "Agent.h"

Agent::Agent(const std::string& name):
	texture(nullptr),
	name(name)
{
}

Agent::~Agent()
{
	if (texture != nullptr) {
		SDL_FreeSurface(texture);
	}
}

bool Agent::loadTexture()
{
	texture = IMG_Load("images/cleaner.png");

	// Check if image loaded.
	if( texture == nullptr )
	{
		setColor(RED);
		std::cout << "ERROR! Could not load vacuum image\n";
		setColor(END);

		return false;
	}

	return true;
}

SDL_Surface* Agent::getTexture() const
{
	return texture;
}

const std::string& Agent::getName() const
{
	return name;
}

HorizontalScanAgent::HorizontalScanAgent():
	Agent("Horizontal scan agent"),
	currentState(WALKING_RIGHT),
	firstMove(true)
{
}

Agent::Action HorizontalScanAgent::perceiveAndAct(Environment::CellState state, Position position)
{
	Action resultAction = NO_OP;

	if (state == Environment::DIRTY) {
		return SUCK;
	}

	if (currentState == WALKING_RIGHT && (firstMove || position != previousLocation)) {
		resultAction = RIGHT;

	} else if (currentState == WALKING_RIGHT && position == previousLocation) {
		resultAction = DOWN;
		currentState = TURNED_RIGHT;

	} else if (currentState == TURNED_RIGHT && position == previousLocation) {
		resultAction = UP;
		currentState = MOVE_TO_TOP_FROM_RIGHT;

	} else if (currentState == TURNED_RIGHT) {
		resultAction = LEFT;
		currentState = WALKING_LEFT;

	} else if (currentState == WALKING_LEFT && (firstMove || position != previousLocation)) {
		resultAction = LEFT;

	} else if (currentState == WALKING_LEFT && position == previousLocation) {
		resultAction = DOWN;
		currentState = TURNED_LEFT;

	} else if (currentState == TURNED_LEFT && position == previousLocation) {
		resultAction = UP;
		currentState = MOVE_TO_TOP_FROM_LEFT;

	} else if (currentState == TURNED_LEFT) {
		resultAction = RIGHT;
		currentState = WALKING_RIGHT;

	} else if ((currentState == MOVE_TO_TOP_FROM_RIGHT || currentState == MOVE_TO_TOP_FROM_LEFT) && position != previousLocation) {
		resultAction = UP;

	} else if (currentState == MOVE_TO_TOP_FROM_RIGHT && position == previousLocation) {
		resultAction = LEFT;
		currentState = WALKING_LEFT;

	}  else if (currentState == MOVE_TO_TOP_FROM_LEFT && position == previousLocation) {
		resultAction = RIGHT;
		currentState = WALKING_RIGHT;
	}

	firstMove = false;
	previousLocation = position;
	return resultAction;
}

void HorizontalScanAgent::reset()
{
	previousLocation = Position(0, 0);
	firstMove = true;
	currentState = WALKING_RIGHT;
}

HorizontalScanAgentPlus::HorizontalScanAgentPlus():
	Agent("Horizontal scan agent plus"),
	rightmost(0),
	leftmost(0),
	topmost(0),
	bottommost(0),
	rightmostSet(false),
	leftmostSet(false),
	topmostSet(false),
	bottommostSet(false),
	currentState(WALKING_RIGHT),
	currentMode(HORIZONTAL_SCAN),
	previousAction(NO_OP),
	firstMove(true),
	upFrom(UPFROM_NONE)
{
}

Agent::Action HorizontalScanAgentPlus::perceiveAndAct(Environment::CellState cellState, Position position)
{
	if (cellState == Environment::DIRTY) {
		return SUCK;
	}

	switch (currentMode) {
	case HORIZONTAL_SCAN:
		return horizontalScanMode(cellState, position);
	case VERTICAL_SCAN:
		return verticalScanMode(cellState, position);
	case ALL_KNOWN:
		return allKnownMode(cellState, position);
	}
}

Agent::Action HorizontalScanAgentPlus::horizontalScanMode(Environment::CellState cellState, Position position)
{
	Action resultAction = NO_OP;

	switch (currentState) {
	case WALKING_RIGHT: {
		if (position == previousLocation && !firstMove) {
			if (!rightmostSet) {
				rightmost = position.column;
				rightmostSet = true;
			}

			resultAction = DOWN;
			currentState = WALKING_LEFT;
			firstMove = true;
		} else {
			resultAction = RIGHT;
			firstMove = false;
		}

		break;
	}

	case WALKING_LEFT: {
		if (position == previousLocation && !firstMove) {
			if (!leftmostSet) {
				leftmost = position.column;
				leftmostSet = true;
			}

			resultAction = DOWN;
			currentState = WALKING_RIGHT;
		} else {
			resultAction = LEFT;
			firstMove = false;
		}

		break;
	}
	}

	if (rightmostSet && leftmostSet) {
		currentMode = VERTICAL_SCAN;
	}

	previousLocation = position;
	previousAction = resultAction;
	return resultAction;
}

Agent::Action HorizontalScanAgentPlus::verticalScanMode(Environment::CellState cellState, Position position)
{
	Action resultAction = NO_OP;

	switch (currentState) {
	case WALKING_RIGHT: {
		if (position == previousLocation && previousAction == DOWN) {
			bottommost = position.row;
			resultAction = UP;
			currentState = WALKING_UP;
			upFrom = UPFROM_LEFT;

		} else if (position.column < rightmost) {
			resultAction = RIGHT;

		} else {
			resultAction = DOWN;
			currentState = WALKING_LEFT;
		}

		break;
	}

	case WALKING_LEFT: {
		if (position == previousLocation && previousAction == DOWN) {
			bottommost = position.row;
			resultAction = UP;
			currentState = WALKING_UP;
			upFrom = UPFROM_RIGHT;

		} else if (position.column > leftmost) {
			resultAction = LEFT;
		} else {
			resultAction = DOWN;
			currentState = WALKING_RIGHT;
		}

		break;
	}

	case WALKING_UP: {
		if (position == previousLocation && previousAction == UP) {
			topmost = position.row;
			currentMode = ALL_KNOWN;
			if (position.column == leftmost) {
				currentState = WALKING_RIGHT;
				resultAction = RIGHT;
			} else {
				currentState = WALKING_LEFT;
				resultAction = RIGHT;
			}
		} else {
			resultAction = UP;
		}

		break;
	}
	}

	previousLocation = position;
	previousAction = resultAction;
	return resultAction;
}

Agent::Action HorizontalScanAgentPlus::allKnownMode(Environment::CellState state, Position position)
{
	Action resultAction = NO_OP;
	int maxLeftmostMovement = leftmost;
	int maxRightmostMovement = rightmost;

	if (position.row > topmost && position.row < bottommost) {
		if (upFrom == UPFROM_LEFT) {
			maxLeftmostMovement++;
		} else if (upFrom == UPFROM_RIGHT) {
			maxRightmostMovement--;
		}
	}

	switch (currentState) {
	case WALKING_RIGHT: {
		if (position.column < maxRightmostMovement) {
			resultAction = RIGHT;
		} else if (position.row == bottommost) {
			resultAction = UP;
			currentState = WALKING_UP;
			upFrom = UPFROM_RIGHT;
		} else {
			resultAction = DOWN;
			currentState = WALKING_LEFT;
		}

		break;
	}

	case WALKING_LEFT: {
		if (position.column > maxLeftmostMovement) {
			resultAction = LEFT;
		} else  if (position.row == bottommost) {
			resultAction = UP;
			currentState = WALKING_UP;
			upFrom = UPFROM_LEFT;
		} else {
			resultAction = DOWN;
			currentState = WALKING_RIGHT;
		}

		break;
	}

	case WALKING_UP: {
		if (position.row > topmost) {
			resultAction = UP;
		} else {
			if (position.column == leftmost) {
				resultAction = RIGHT;
				currentState = WALKING_RIGHT;
			} else {
				resultAction = LEFT;
				currentState = WALKING_LEFT;
			}
		}

		break;
	}
	}

	return resultAction;
}

void HorizontalScanAgentPlus::reset()
{
	rightmost = 0;
	leftmost = 0;
	topmost = 0;
	bottommost = 0;
	rightmostSet = false;
	leftmostSet = false;
	topmostSet = false;
	bottommostSet = false;
	previousLocation = Position(0, 0);
	currentState = WALKING_RIGHT;
	currentMode = HORIZONTAL_SCAN;
	previousAction = NO_OP;
	firstMove = true;
	upFrom = UPFROM_NONE;
}

ReflexAgent::ReflexAgent() :
		Agent("Reflex agent")
{
}

Agent::Action ReflexAgent::perceiveAndAct(Environment::CellState state, Position position)
{
	if( state == Environment::DIRTY )
		return SUCK;
	else if( position.column == 0 )
		return RIGHT;
	else if( position.column == 1 )
		return LEFT;

	return NO_OP;
}

void ReflexAgent::reset()
{
}

ReflexStateAgent::ReflexStateAgent():
	cellAClean(false),
	cellBClean(false),
	Agent("Reflex state agent")
{
}

Agent::Action ReflexStateAgent::perceiveAndAct(Environment::CellState state, Position position)
{
	Action resultAction = NO_OP;
	
	if( state == Environment::DIRTY ) {
		if (cellAClean && position.column == 0) {
			cellAClean = true;
		} else if (cellBClean && position.column == 1) {
			cellBClean = true;
		}
		resultAction =  SUCK;
	} else if( position.column == 0 ) {
		if (cellAClean == false) {
			cellAClean = true;
		}
		resultAction = RIGHT;
	} else if( position.column == 1 ) {
		if (cellBClean == false) {
			cellBClean = true;
		}
		resultAction = LEFT;
	}

	if (cellAClean && cellBClean)
		resultAction = NO_OP;

	return resultAction;
}

void ReflexStateAgent::reset()
{
	cellAClean = false;
	cellBClean = false;
}

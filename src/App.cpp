#include "App.h"
#include "Utils.h"
#include <cstdlib>

App::App():
	m_screenWidth(640),
	m_screenHeight(480),
	m_framesPerSecond(60),
	m_prevFrameStartTime(0),
	m_curFrameStartTime(0),
	m_fpsController(true),
	m_simulationSteps(1000),
	m_renderOn(true),
	m_respawnDirt(false),
	m_respawnDirtProbability(3),
	m_screen(nullptr),
	m_environment(2, 1),
	m_agent(nullptr)
{
}

App::~App()
{
	// Shutdown SDL
	SDL_Quit();
}

void App::handleResize(const int width, const int height)
{
	m_screen = SDL_SetVideoMode(width, height, 32, SDL_SWSURFACE | SDL_DOUBLEBUF | SDL_RESIZABLE);
}

void App::runSimulation()
{
	int totalPerformancePoints = 0;
	int totalMoves = 0;
	unsigned int step = 0;
	bool quit = false;

	if (m_renderOn) {
		initSdl();
		render();
	}

	while (quit == false && step < m_simulationSteps)
	{
		if (m_renderOn) {
			SDL_Event sdlEvent;
			while( SDL_PollEvent( &sdlEvent ) )
			{
				if( sdlEvent.type == SDL_QUIT )
					quit = true;

				if( sdlEvent.type == SDL_KEYDOWN )
				{
					if( sdlEvent.key.keysym.sym == SDLK_ESCAPE )
						quit = true;

					if (sdlEvent.key.keysym.sym == SDLK_s) {
						quit = true;
					}
				}

				if (sdlEvent.type == SDL_VIDEORESIZE) {
					handleResize(sdlEvent.resize.w, sdlEvent.resize.h);
				}
			}
		}

		totalMoves += m_environment.update();
		int performancePoints = m_environment.getNumCleanCells();
		totalPerformancePoints += performancePoints;
		std::cout << "Points: " << performancePoints << std::endl;

		if (m_renderOn) {
			render();
		}

		if (m_respawnDirt) {
			m_environment.placeRandomDirt(m_respawnDirtProbability);
		}

		step++;
	}

	setColor(BLUE);
	std::cout << "\nSteps: " << step << std::endl;

	std::cout << "\nPerformance score (+1 point per clean cell)\n";
	std::cout << "Total score: " << totalPerformancePoints << std::endl;
	std::cout << "Average point per step: " << float(totalPerformancePoints) / float(m_simulationSteps) << std::endl;

	std::cout << "\nModified performance score (+1 point per clean cell, -1 point per move)\n";
	std::cout << "Total score: " << totalPerformancePoints - totalMoves << std::endl;
	std::cout << "Average point per step: " << float(totalPerformancePoints - totalMoves) / float(m_simulationSteps) << std::endl;
	setColor(END);

	m_agent->reset();
	m_environment.setAgentPosition(Position(0, 0));
	if (m_renderOn) {
		SDL_Quit();
	}
}

void App::render()
{
	if (m_fpsController) {
		m_curFrameStartTime = SDL_GetTicks();

		if (m_prevFrameStartTime == 0)
			m_prevFrameStartTime = m_curFrameStartTime;
	}

	// Draw
	//------------------------------------------------------------------------
	SDL_FillRect(m_screen, NULL, (Uint32(255) << 24) + (Uint32(200) << 16) + (Uint32(200) << 8) + Uint32(200));

	m_environment.draw(m_screen);

	// Update Screen
	SDL_Flip(m_screen);

	// Regulate the frame rate
	//------------------------------------------------------------------------
	if (m_fpsController) {
		Uint32 frameTime = SDL_GetTicks() - m_curFrameStartTime;

		if (frameTime < (1000 / m_framesPerSecond))
			SDL_Delay((1000 / m_framesPerSecond) - frameTime);

		m_prevFrameStartTime = m_curFrameStartTime;
	}
}

void App::placeRandomDirt()
{
	int probability;

	if( scanf("%i", &probability) != 1 )
	{
		setColor(BLUE);
		std::cout << "'randomdirt' needs to be assigned an integer vaule.\n";
		setColor(END);

		return;
	}

	setColor(GREEN);
	std::cout << "Random dirt = " << probability << "\n";
	setColor(END);

	m_environment.placeRandomDirt(probability);
}

void App::setVacuumerPosition()
{
	Position pos;

	if( scanf("%i %i", &pos.row, &pos.column) != 2 )
	{
		setColor(BLUE);
		std::cout << "'setpos' needs to be assigned two integers.\n";
		setColor(END);
		return;
	}

	setColor(GREEN);
	std::cout << "Agents position is " << pos.column << "(column) " << pos.row << "(row)\n";
	setColor(END);

	m_environment.setAgentPosition(pos);
}

void App::init()
{
	m_agent = new HorizontalScanAgentPlus();
	m_environment.setAgent(m_agent);
	m_environment.setAgentPosition(Position(0, 0));
}

void App::set_screen_height()
{
	if( scanf("%i", &m_screenHeight) != 1 )
	{
		setColor(RED);
		std::cout << "'screenheight' needs to be assigned an integer vaule.\n";
		setColor(END);
		return;
	}

	setColor(GREEN);
	std::cout << "Screen height = " << m_screenHeight << "\n";
	setColor(END);
}

void App::set_agent()
{
	std::string line;

	std::cin >> line;

	if( line == "reflex" )
	{
		delete m_agent;
		m_agent = new ReflexAgent();
		m_environment.setAgent(m_agent);

		setColor(GREEN);
		std::cout << "Current agent is " << line << "\n";
		setColor(END);
	}
	else if( line == "reflexstate" )
	{
		delete m_agent;
		m_agent = new ReflexStateAgent();
		m_environment.setAgent(m_agent);

		setColor(GREEN);
		std::cout << "Current agent is " << line << "\n";
		setColor(END);
	}
	else if( line == "horizontalscan")
	{
		delete m_agent;
		m_agent = new HorizontalScanAgent();
		m_environment.setAgent(m_agent);

		setColor(GREEN);
		std::cout << "Current agent is " << line << "\n";
		setColor(END);
	}
	else if( line == "horizontalscan")
	{
		delete m_agent;
		m_agent = new HorizontalScanAgent();
		m_environment.setAgent(m_agent);

		setColor(GREEN);
		std::cout << "Current agent is " << line << "\n";
		setColor(END);
	}
	else if ( line == "horizontalscanplus") {
		delete m_agent;
		m_agent = new HorizontalScanAgentPlus();
		m_environment.setAgent(m_agent);

		setColor(GREEN);
		std::cout << "Current agent is " << line << "\n";
		setColor(END);
	}
	else
	{
		setColor(BLUE);
		std::cout << "Could not find " << line << "\n"
				"Use the command ";
		setColor(END);

		setColor(CYAN);
		std::cout << "agentlist ";
		setColor(END);

		setColor(BLUE);
		std::cout << "for a list of available agents\n";
		setColor(END);
	}
}

void App::set_screen_width()
{
	if( scanf("%i", &m_screenWidth) != 1 )
	{
		setColor(RED);
		std::cout << "'screenwidth' needs to be assigned an integer vaule.\n";
		setColor(END);
		return;
	}

	setColor(GREEN);
	std::cout << "Screen width = " << m_screenWidth << "\n";
	setColor(END);
}

int App::execute(const std::string& configFileName)
{
	init();

	if( !load_config(configFileName) )
	{
		if( configFileName == "default.conf")
		{
			create_default_config("default.conf");
			load_config("default.conf");
		}
		else if (!load_config("default.conf"))
		{
			create_default_config("default.conf");
			load_config("default.conf");
		}
	}

	display_status();

	std::string input;
	bool running = true;

	while (running) {
		std::cout << "> ";
		std::cin >> input;

		if (input == "agentlist" ) {
			display_agent_list();

		} else if (input == "cleanenv") {
			m_environment.cleanMap();

		} else if (input == "dirt") {
			placeDirt();

		} else if (input == "envsize") {
			changeEnvironmentSize();

		} else if (input == "exit") {
			running = false;

		} else if (input == "loadconfig") {
			std::string fileName;
			std::cin >> fileName;
			load_config(fileName);

		} else if (input == "loadmap") {
			std::string fileName;
			std::cin >> fileName;
			m_environment.load_map(fileName);

		} else if (input == "randomdirt") {
			placeRandomDirt();

		} else if (input == "saveconfig") {
			std::string fileName;
			if (std::cin.peek() == '\n') {
				fileName = "default.conf";
			} else {
				std::cin >> fileName;
			}
			save_config(fileName);

		} else if (input == "savemap") {

		} else if (input == "screenheight") {
			set_screen_height();

		} else if (input == "screenwidth") {
			set_screen_width();

		} else if (input == "setagent") {
			set_agent();

		} else if (input == "setfps") {
			set_fps();

		} else if (input == "setpos") {
			setVacuumerPosition();

		} else if (input == "setrespawnprob") {
			set_respawn_probability();

		} else if (input == "setsteps") {
			set_steps();

		} else if (input == "start") {
			runSimulation();

		} else if (input == "status") {
			display_status();

		} else if (input == "togglefps") {
			toggle_fps();

		} else if (input == "togglerender") {
			toggle_render();

		} else if (input == "togglerespawn") {
			m_respawnDirt = !m_respawnDirt;

//		} else if (input == "togglesound") {
//			m_agent.toggleSound();

		} else if (input == "help") {
			setColor(CYAN);
			std::cout << "agentlist";
			setColor(END);

			std::cout << " - Displays a list of available agents\n";

			setColor(CYAN);
			std::cout << "cleanenv";
			setColor(END);

			std::cout << " - Cleans the environment\n";

			setColor(CYAN);
			std::cout << "dirt";
			setColor(END);

			std::cout << " - Place dirt in a specified column and row\n";

			setColor(CYAN);
			std::cout << "envsize";
			setColor(END);

			std::cout << " - Specify environment size by rows and colums\n";

			setColor(CYAN);
			std::cout << "exit";
			setColor(END);

			std::cout << " - Exit the program\n";

			setColor(CYAN);
			std::cout << "loadconfig";
			setColor(END);

			std::cout << " - Load setup from a specified file\n";

			setColor(CYAN);
			std::cout << "loadmap";
			setColor(END);

			std::cout << " - Loads map from file\n";

			setColor(CYAN);
			std::cout << "randomdirt";
			setColor(END);

			std::cout << " - Spawn random dirt with specified percentage of dirt spawning 	\n";

			setColor(CYAN);
			std::cout << "saveconfig";
			setColor(END);

			std::cout << " - Save current setup to a specified file\n";

			setColor(CYAN);
			std::cout << "screenheight";
			setColor(END);

			std::cout << " - Specify the height of simulation screen\n";

			setColor(CYAN);
			std::cout << "screenwidth";
			setColor(END);

			std::cout << " - Specify the width of simulation screen\n";

			setColor(CYAN);
			std::cout << "setagent";
			setColor(END);

			std::cout << " - Specify type of agent to use in simulation\n";

			setColor(CYAN);
			std::cout << "setfps";
			setColor(END);

			std::cout << " - Specify how many frames to render per second\n";

			setColor(CYAN);
			std::cout << "setpos";
			setColor(END);

			std::cout << " - Specify the vacuum cleaners starting position\n";

			setColor(CYAN);
			std::cout << "setrespawnprob";
			setColor(END);

			std::cout << " - Specify re-spawning probability of dirt\n";

			setColor(CYAN);
			std::cout << "setsteps";
			setColor(END);

			std::cout << " - Specify steps to simulate\n";

			setColor(CYAN);
			std::cout << "start";
			setColor(END);

			std::cout << " - Start the simulation\n";

			setColor(CYAN);
			std::cout << "status";
			setColor(END);

			std::cout << " - Display current status of commands\n";

			setColor(CYAN);
			std::cout << "togglefps";
			setColor(END);

			std::cout << " - Toggle frames per second ON/OFF\n";

			setColor(CYAN);
			std::cout << "togglerender";
			setColor(END);

			std::cout << " - Toggle graphical visualization ON/OFF\n";

			setColor(CYAN);
			std::cout << "togglerespawn";
			setColor(END);

			std::cout << " - toggle respawning of dirt ON/OFF\n";

			//setColor(CYAN);
			//std::cout << "togglesound";
			//setColor(END);

			//std::cout << " - Toggle the sound ON/OFF\n";

		} else {
			setColor(RED);
			std::cout << "Invalid command.\n";
			setColor(END);
		}

		// In case of a valid command following an invalid command ignore it.
		// Eg: random dirt, random(invalid) dirt(valid).
		std::cin.ignore(256,'\n');
	}

	return 0;
}

void App::display_agent_list()
{
	setColor(BLUE);
	std::cout << "Available agents are:\n";
	setColor(END);

	setColor(CYAN);
	std::cout << "reflex";
	setColor(END);
	std::cout << " - A simple reflex agent moving between two cells sucking dirt.\n";
	
	setColor(CYAN);
	std::cout << "reflexstate";
	setColor(END);
	std::cout << " - A simple reflex agent with a state moving between two cells sucking dirt and stopping when clean.\n";

	setColor(CYAN);
	std::cout << "horizontalscan";
	setColor(END);
	std::cout << " - A bit more complex agent that can move in a bigger environment.\n";

	setColor(CYAN);
	std::cout << "horizontalscanplus";
	setColor(END);
	std::cout << " - A even more complex agent that can move in a bigger environment using less moves.\n";
}
void App::toggle_render()
{
	m_renderOn = !m_renderOn;

	if( m_renderOn )
	{
		setColor(GREEN);
		std::cout << "Rendering is ON!\n";
		setColor(END);
	}
	else
	{
		setColor(GREEN);
		std::cout << "Rendering is OFF!\n";
		setColor(END);
	}
}

void App::set_steps()
{
	int steps;

	if( scanf("%i", &steps) != 1 )
	{
		setColor(RED);
		std::cout << "'setsteps' needs to be assigned an integer.\n";
		setColor(END);
		return;
	}

	setColor(GREEN);
	std::cout << "Steps is set to = " << steps << "\n";
	setColor(END);

	m_simulationSteps = steps;
}
void App::set_fps()
{
	int fpsFromInput;

	if( scanf("%i", &fpsFromInput) != 1 )
	{
		setColor(RED);
		std::cout << "'setfps' needs to be assigned an integer vaule.\n";
		setColor(END);
		return;
	}

	m_framesPerSecond = fpsFromInput;

	setColor(GREEN);
	std::cout << "Fps = " << m_framesPerSecond << "\n";
	setColor(END);

	if( !m_fpsController )
	{
		setColor(YELLOW);
		std::cout << "\nWARNING: FPS Controller is currently turned OFF!\n"
					"Use the command 'togglefps' to turn it back ON!\n";
		setColor(END);
	}
}

void App::toggle_fps()
{
	m_fpsController = !m_fpsController;

	if( m_fpsController )
	{
		setColor(GREEN);
		std::cout << "FPS Controller is ON!\n";
		setColor(END);
	}
	else
	{
		setColor(GREEN);
		std::cout << "FPS Controller is OFF!\n";
		setColor(END);
	}
}

void App::changeEnvironmentSize()
{
	int rows, columns;

	if( scanf("%i %i", &rows, &columns) != 2 )
	{
		setColor(RED);
		std::cout << "'envsize' needs to be assigned two integers.\n";
		setColor(END);
		return;
	}

	m_environment.setSize(columns, rows);
}


void App::placeDirt()
{
	int row, column;

	if (scanf("%i %i", &row, &column) != 2)
	{
		setColor(RED);
		std::cout << "'dirt' needs to be assigned two integers.\n";
		setColor(END);
		return;
	}

	m_environment.setDirtyCell(column, row);
}

void App::set_respawn_probability()
{
	int probability;

	if( scanf("%i", &probability) != 1 )
	{
		setColor(RED);
		std::cout << "'setrespawnprob' needs to be assigned an integer.\n";
		setColor(END);
		return;
	}

	m_respawnDirtProbability = probability;
}

void App::display_status()
{
	// Status
	setColor(BLUE);
	std::cout << "Status\n";
	setColor(END);

	// Current config file used
	setColor(CYAN);
	std::cout << "Current config file";
	setColor(END);

	std::cout << " = ";

	setColor(GREEN);
	std::cout << m_currentConfigFile << "\n";
	setColor(END);

	// Current agent used
	setColor(CYAN);
	std::cout << "Current agent";
	setColor(END);

	std::cout << " = ";

	setColor(GREEN);
	std::cout << m_agent->getName() << "\n";
	setColor(END);

	// Render toggle
	setColor(CYAN);
	std::cout << "Render toggle";
	setColor(END);

	std::cout << " = ";

	setColor(GREEN);

	if(m_renderOn )
		std::cout << "TRUE" << "\n";
	else
		std::cout << "FALSE" << "\n";

	setColor(END);

	// Screen width
	setColor(CYAN);
	std::cout << "Screen width";
	setColor(END);

	std::cout << " = ";

	setColor(GREEN);
	std::cout << m_screenWidth << "\n";
	setColor(END);

	// Screen height
	setColor(CYAN);
	std::cout << "Screen height";
	setColor(END);

	std::cout << " = ";

	setColor(GREEN);
	std::cout << m_screenHeight << "\n";
	setColor(END);

	// FPS toggle
	setColor(CYAN);
	std::cout << "FPS toggle";
	setColor(END);

	std::cout << " = ";

	setColor(GREEN);

	if( m_fpsController )
		std::cout << "TRUE" << "\n";
	else
		std::cout << "FALSE" << "\n";

	setColor(END);

	//FPS
	setColor(CYAN);
	std::cout << "FPS";
	setColor(END);

	std::cout << " = ";

	setColor(GREEN);
	std::cout << m_framesPerSecond << "\n";
	setColor(END);

	// Simulation steps
	setColor(CYAN);
	std::cout << "Simulation steps";
	setColor(END);

	std::cout << " = ";

	setColor(GREEN);
	std::cout << m_simulationSteps << "\n";
	setColor(END);

	// Respawn dirt
	setColor(CYAN);
	std::cout << "Respawn dirt";
	setColor(END);

	std::cout << " = ";

	setColor(GREEN);

	if( m_respawnDirt )
		std::cout << "TRUE" << "\n";
	else
		std::cout << "FALSE" << "\n";

	setColor(END);

	// Dirt probability
	setColor(CYAN);
	std::cout << "Dirt probability";
	setColor(END);

	std::cout << " = ";

	setColor(GREEN);
	std::cout << m_respawnDirtProbability << "\n";
	setColor(END);

//	m_agent.display_status();
	m_environment.display_status();

	//--------------------------------
	setColor(BLUE);
	std::cout << "Enter the command 'help' for a list of available commands\n";
	setColor(END);
}

bool App::initSdl()
{
	// Initialize SDL
	if( SDL_Init(SDL_INIT_EVERYTHING) == -1 )
		return false;

	handleResize(m_screenWidth, m_screenHeight);

	if( m_screen == nullptr )
	{
		setColor(RED);
		std::cout << "ERROR! Could not setup screen\nExiting\n";
		setColor(END);

		return false;
	}

	if( !m_agent->loadTexture() )
	{
		return false;
	}

	if( !m_environment.load_assets() )
	{
		setColor(RED);
		std::cout << "ERROR! Failed to load environment assets\nExiting\n";
		setColor(END);

		return false;
	}

	return true;
}

bool App::load_config(std::string fileName)
{
	std::string line;

	std::ifstream infile(fileName);

	setColor(BLUE);
	std::cout << "Loading " << fileName << "...\n";
	setColor(END);

	if( !infile )
	{
		infile.close();

		setColor(RED);
		std::cout << "ERROR! Could not load " << fileName << " since it does not exist\n";
		setColor(END);

		return false;
	}

	infile >> line;

	while( infile )
	{
		if( line[0] == '#')
		{
			infile.ignore(256, '\n');
		}
		else if( line == "fps" )
		{
			if( !(infile >> m_framesPerSecond) )
			{
				infile.close();
				return false;
			}
		}
		else if( line == "fpsToggle" )
		{
			infile >> line;

			if( line == "TRUE")
				m_fpsController = true;
			else if( line == "FALSE")
				m_fpsController = false;
			else
			{
				infile.close();
				return false;
			}
		}
		else if( line == "screenWidth" )
		{
			if( !(infile >> m_screenWidth) )
			{
				infile.close();
				return false;
			}
		}
		else if( line == "screenHeight" )
		{
			if( !(infile >> m_screenHeight) )
			{
				infile.close();
				return false;
			}
		}
		else if( line == "render")
		{
			infile >> line;

			if( line == "TRUE")
				m_renderOn = true;
			else if( line == "FALSE")
				m_renderOn = false;
			else
			{
				infile.close();
				return false;
			}
		}
		else if( line == "spawnDirtAtStartProbability")
		{
			int spawnDirtAtStartProbability;

			if( !(infile >> spawnDirtAtStartProbability))
			{
				infile.close();
				return false;
			}

			m_environment.placeRandomDirt(spawnDirtAtStartProbability);
		}
		else if( line == "agentPosition")
		{
			Position position;

			if( !(infile >> position.row >> position.column ) )
			{
				infile.close();
				return false;
			}
			else
			{
				m_environment.setAgentPosition(position);
			}
		}
		else if( line == "agentType" )
		{
			infile >> line;

			if( line == "reflexAgent")
			{
				delete m_agent;
				m_agent = new ReflexAgent();
				m_environment.setAgent(m_agent);
			}
			else if( line == "reflexStateAgent")
			{
				delete m_agent;
				m_agent = new ReflexStateAgent();
				m_environment.setAgent(m_agent);
			}
			else if( line == "horizontalScan")
			{
				delete m_agent;
				m_agent = new HorizontalScanAgent();
				m_environment.setAgent(m_agent);
			}
			else if( line == "horizontalScanPlus")
			{
				delete m_agent;
				m_agent = new HorizontalScanAgentPlus();
				m_environment.setAgent(m_agent);
			}
			else
			{
				infile.close();
				return false;
			}
		}
		else if( line == "dirtRespawn")
		{
			infile >> line;

			if( line == "TRUE")
				m_respawnDirt = true;
			else if( line == "FALSE")
				m_respawnDirt = false;
			else
			{
				infile.close();
				return false;
			}
		}
		else if( line == "dirtProbability")
		{
			if( !(infile >> m_respawnDirtProbability) )
			{
				infile.close();
				return false;
			}
		}
		else if( line == "environmentSize")
		{
			int rows, columns;
			if (!(infile >> rows >> columns)) {
				infile.close();
				return false;
			}
			m_environment.setSize(columns, rows);
		}

		infile >> line;
	}

	m_currentConfigFile = fileName;

	return true;
}

void App::create_default_config(std::string fileName)
{
	setColor(RED);
	std::cout << "Creating a new from default settings\n";
	setColor(END);

	std::ofstream outfile(fileName);

	outfile << "# Config file for for AI Assignment 1\n"
			"\n"
			"screenWidth 640\t\t#Default 640\n"
			"screenHeight 480\t#Default 480\n"
			"fps 60\t\t\t#Default 60\n"
			"fpsToggle TRUE\t\t#Default TRUE\n"
			"render TRUE\t\t#Default TRUE\n"
			"agentType reflexAgent\t\t#Default reflexAgent\n"
			"agentPosition 1 1\t#Default 1 1\n"
			"dirtRespawn FALSE\t#Default FALSE\n"
			"dirtProbability 3\t#Default 3\n"
			"environmentSize 4 6\t#(row column)\n"
			"spawnDirtAtStartProbability 80\t#Default 80\n"
			"\n"
			;

	outfile.close();
}

void App::save_config(std::string fileName)
{
	std::ifstream infile(fileName);

	if( infile )
	{
		char choice;

		setColor(YELLOW);
		std::cout << "WARNING: The file \"" << fileName << "\" already exist\n"
				"Are you sure you want to overwrite it? [Y/n]\n";
		setColor(END);

		std::cin.ignore();
		std::cout << "> ";

		scanf("%c", &choice);

		if( choice != 'y' && choice != 'Y' )
		{
			setColor(BLUE);
			std::cout << "File will not be overwritten\n";
			setColor(END);
			return;
		}

	}

	std::ofstream outfile(fileName);

	outfile << "# Config file for for AI Assignment 1\n";;
	outfile << "\n";

	outfile << "fps "<< m_framesPerSecond << "\t\t\t#Default 60\n";

	outfile << "fpsToggle ";

	if( m_fpsController )
		outfile << "TRUE";
	else
		outfile << "FALSE";

	outfile << "\t\t#Default TRUE\n";

	outfile << "render ";

	if( m_renderOn )
		outfile << "TRUE";
	else
		outfile << "FALSE";

	outfile << "\t\t#Default TRUE\n";

	outfile << "dirtRespawn ";

	if( m_respawnDirt )
		outfile << "TRUE";
	else
		outfile << "FALSE";

	outfile << "\t#Default FALSE\n";

	outfile << "dirtProbability " << m_respawnDirtProbability << "\t#Default 3\n";

	m_environment.save_config(outfile);

	setColor(BLUE);
	std::cout << "Successful save\n";
	setColor(END);

//	m_agent.save_config(outfile);

}


#ifndef ENVIRONMENT_H_
#define ENVIRONMENT_H_

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <list>
#include "Utils.h"
#include <fstream>
class Agent;

class Environment {
public:
	enum CellState { CLEAN, DIRTY, WALL };

	Environment(int mapWidth, int mapHeight);
	virtual ~Environment();

	bool load_assets();
	void draw(SDL_Surface* screen) const;
	/**
	 * Lets the agent perceive and act upon this.
	 * @return Number of moves taken by the agent.
	 */
	int update();

	void setCleanCell(int column, int row);
	void setDirtyCell(int column, int row);
	void setSize(int columns, int rows);
	void placeRandomDirt(const int probability);
	void cleanMap();
	void setAgent(Agent* agent);
	void setAgentPosition(const Position position);

	bool hasDirtyCell(int column, int row) const;
	bool isValidCell(const int row, const int column) const;
	int getNumCleanCells() const;
	void display_status();
	bool load_config(std::ifstream& infile, std::string line);
	void save_config(std::ofstream& outfile);
	void load_map(const std::string& fileName);

private:
	int numColumns;
	int numRows;
	CellState** map;
	SDL_Surface* cellImage;
	SDL_Surface* dirtImage;
	SDL_Surface* wallImage;
	float cellWidth;
	float cellHeight;
	Agent* agent;
	Position agentPos;

	void deleteMap();
	void createMap(int columns, int rows);
	void drawObject(SDL_Surface* object, SDL_Surface* screen, Position position) const;
	Position getRandomMapPosition() const;
};

#endif /* ENVIRONMENT_H_ */
